package com.exam.examportal;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.exam.examportal.helper.UserFoundException;
import com.exam.examportal.model.Role;
import com.exam.examportal.model.User;
import com.exam.examportal.model.UserRole;
import com.exam.examportal.service.UserService;

@SpringBootApplication
@CrossOrigin(origins = "*")
public class ExamportalApplication implements CommandLineRunner{
	
	@Autowired
	UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(ExamportalApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Project Running");
        /*User user=new User();
        user.setFirstName("Ghassen");
        user.setLastName("Msahel");
        user.setEmail("ghassenmsahel2010@gmail.com");
        user.setPassword("22848497");
        user.setUsername("ghass123");
        user.setPhone("99439604");
        user.setProfile("default.png");

        Role role = new Role();
        role.setRoleId(44L);
        role.setRoleName("ADMIN");
        Set<UserRole> userRoleSet=new HashSet<>();
        UserRole userRole=new UserRole();
        userRoleSet.add(userRole);
        userRole.setRole(role);
        userRole.setUser(user);
        User user1 = this.userService.createUser(user,userRoleSet);
        System.out.println(user1);*/
		

		
	}

}
